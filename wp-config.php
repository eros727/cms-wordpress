<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lm$Hf4i^T[3<l*Y`7jBM_ $,v;AaU6[}if<XCS{t=ktEVhD`Ub.|R7EV_i%3yV1;' );
define( 'SECURE_AUTH_KEY',  'sQv>qTBi(U|@Tb6%a$;y&gLBJ_xJkS9}]m-!1G~4c=wY]rQAi*5FEmP+u/6Cc {j' );
define( 'LOGGED_IN_KEY',    'q!<%kD3Cd!@+JJ|A;=6ttO|?ap2 DhDfEsbmn@io$=u~6+P%CrkHa8%-/,){g(R3' );
define( 'NONCE_KEY',        '}QaSSH7NWtT;#8$K6_bj~jT(8]-0RK:yHq,ke6J/Q8/b]P!v>C@^;~sy`nv<Tf#m' );
define( 'AUTH_SALT',        '0BKJ!!D!U7Y(mTZPV3u+pxMcI;JDxNM3Tgr SPlWRFz@<#/^gyq3_k&j@LRf0{|6' );
define( 'SECURE_AUTH_SALT', 'uBNIyI#w3gV+i1@M)4}7{k*:U1&P.)<f)uA)jILR9Y/5XZY>Fnu,QFk>2yTZ04V$' );
define( 'LOGGED_IN_SALT',   '@G>p,+*A/>>bRC:*ToS(cZqQpuHS$o ZLGtJ^#H9)<2T#wfgnG9;5!I]FpJ|=9$a' );
define( 'NONCE_SALT',       'VWS#Qv~LeA{xoM+:s4@<~r6+6pU.{E/x^:;6m3|>sXz&C=,v?Z7mEOOApr(zqmQ;' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
