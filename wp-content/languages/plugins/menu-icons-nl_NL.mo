��    8      �  O   �      �     �  4   �     !  B   8  D   {     �     �     �     �     �     �     �     �  .   �  
   -  	   8  	   B     L     S  
   c     n  
   s     ~  
   �     �  T   �     �  
   �     �  +        <     C  �   F                (     /  	   5     ?     M     T  9   `     �     �     �     �  	   �  	   �     �     �     �     �  )   �     	     )	  ,  8	     e
  >   |
     �
  I   �
  F        f     i  	   o     y     ~     �     �     �  8   �  	   �     �     �               +  	   ;     E     U     \     o  s   u  	   �  
   �     �  =        X     _  �   c                .     :     C     O  
   d     o  >        �     �     �     �  	   �  	   �     �     �            )        B     X         7       "            (             $         %   !          *              '   3         -              )   4      ,          1   /   #       	   +                     0      .          2      5                      
      8   6                                      &              "%s" menu settings %1$s: Type %2$s is not supported, reverting to text. &mdash; Select &mdash; <strong>Menu Icons Settings</strong> have been successfully reset. <strong>Menu Icons Settings</strong> have been successfully updated. After All Baseline Before Bottom Change Current Menu Deselect Discard all changes and reset to default state Extensions Font Size Full Size Global Global settings Hide Label Icon Icon Types Icon: Image Size Large Looks like Menu Icons was installed via Composer. Please activate Icon Picker first. Medium Menu Icons Menu Icons Settings Menu Icons: No registered icon types found. Middle No Please note that the actual look of the icons on the front-end will also be affected by the style of your active theme. You can add your own CSS using %2$s or a plugin such as %3$s if you need to override it. Position Preview Remove Reset SVG Width Save Settings Select Select Icon Spice up your navigation menus with pretty icons, easily. Sub Super Text Bottom Text Top ThemeIsle Thumbnail Top Type Vertical Align Yes https://github.com/Codeinwp/wp-menu-icons https://themeisle.com the customizer PO-Revision-Date: 2022-11-16 09:04:40+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/4.0.0-alpha.3
Language: nl
Project-Id-Version: Plugins - Menu Icons by ThemeIsle - Development (trunk)
 "%s" menu instellingen %1$s: Type %2$s wordt niet ondersteund, teruggezet naar tekst. &mdash; Selecteer &mdash; <strong>Menu Pictogrammen Instellingen</strong> zijn met  succes gereset. <strong>Menu Pictogram Instellingen</strong> zijn succesvol ge-update. Na Alles Basislijn Voor Onder Wijzigen Huidig menu Deselecteren Wijzigingen negeren en standaard instellingen herstellen Extensies Lettertype grootte Volledige grootte Globaal Globale instellingen Label verbergen Pictogram Pictogram types Icoon: Afbeeldingsgrootte Groot Het lijkt er op dat “Menu Icons” geïnstalleerd werd via Composer. Activeer alsublieft eerst “Icon Picker”. Gemiddeld Menu Icons Menu Pictogram Instellingen Menu pictogram: Geen geregistreerde pictogram types gevonden. Midden Nee De uitstraling van de pictogrammen worden beïnvloed door de stijl van je actieve thema. Je kunt je eigen CSS code gebruiken met %2$s of een plugin zoals %3$s om dit aan te passen. Positie Voorvertoning Verwijderen Resetten SVG breedte Instellingen opslaan Selecteren Selecteer icoon Fleur eenvoudig je navigatie menu's op met mooie pictogrammen. Sub Super Tekst beneden Tekst boven ThemeIsle Thumbnail Boven Type Verticaal uitlijnen Ja https://github.com/Codeinwp/wp-menu-icons https://themeisle.com de customizer 